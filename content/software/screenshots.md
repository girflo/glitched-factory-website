+++
title = "Screenshots"
weight = 3
[extra]
description = "Screenshots of the software"
+++

You can find here a screenshot of all the tabs of this sofware. The default theme is Dark but you can switch to the Light one by cliking the ☼ icon and the ☽ to go back to the Dark one.

The screenshots present on this page are from the macos version but the linux and windows look exactly the same.

## Dark theme

{{ screenshots_gallery(image_path_1="/images/screenshots/dark1", image_path_2="/images/screenshots/dark2", image_path_3="/images/screenshots/dark3", image_path_4="/images/screenshots/dark4", alt="Glitched factory screenshot dark theme 4") }}

## Light theme

{{ screenshots_gallery(image_path_1="/images/screenshots/light1", image_path_2="/images/screenshots/light2", image_path_3="/images/screenshots/light3", image_path_4="/images/screenshots/light4") }}
