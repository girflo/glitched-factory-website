+++
title = "Documentation"
weight = 2
[extra]
description = "How to use this software"
+++

## Queue

The queue is not really a queue, because a queue is supposed to handle one task at a time. In the previous version of this software, it was the case. Since the version 0.3.0 multiple tasks are handled at the same time. This panel shows you the current state of the tasks you started.

Glitching entire folders can take a lot of time, so every job started will be visible in the panel. Each line represents a task you started, newer tasks are displayed at the top of the panel.

A line is composed of two elements :

- The kind of task: algorithm name, sample or convert
- The state of the task: ongoing or finished

## Glitcher

### Input

This section is for selecting the image(s) that you want to glitch. The Glitched Factory can only glitch PNG images.
To select an image to glitch click on `Open file...` and inside of the new windows browse to the specific image and open it.

If you want to use the same glitch on multiple images you can create a directory containing all the PNG images and click on `Open folder`, select the directory in the new windows and open it. Only the direct children are used, so if this folder contains other folder they will be ignored.

### Output

This section is for specifying the where to write the newly glitched image. To specify the folder where you want to write your new image, click on Output directory and select this directory.
When selected the output folder path will be displayed.

The filename defines the name of the output: the index is a number and is followed by the value of the field. When glitching entire folder the files are read in alphabetical order and the output keeps this order.

### Algorithms - Brush

Copy a pixel color in diagonal.

Brush direction: the direction of the brush.

Probability: the probability on which each pixel is brushed.

Min: the minimum amount of pixel brushed at a time.

Max: the maximum amount of pixel brushed at a time.

### Algorithms - Duplicate

For each occurrence this algorithm will copy data from another part of the image, then force the paying to use a given filter.

Filter: Png filter used when writing the image

Repetition: Number of times the duplication happens, the distance between the repetition is always the same.

Gap: Initial offset for the first duplication.

### Algorithms - Replace

For each occurrence this algorithm will replace the data with 0, then force the paying to use a given filter.

Filter: Png filter used when writing the image

Repetition: Number of times the replacing happens, the distance between the repetition is always the same.

### Algorithms - Sort (brut)

Sort the pixels of the image according to a given attribute. When using the brut sort then entire line or column will be sorted.

Direction: the direction of the sorting, either left to right either top to bottom.

### Algorithms - Sort (smart)

Sort the pixels of the image according to a given attribute. When using the smart sort groups of colors is created and their content is sorted separately.

Inverted sorting: inverse the sorting (for example: light to dark become dark to light).

Direction: the direction of the sorting, either left to right either top to bottom.

Sort by:

- Hue: the pixel with the bigger hue will be at the beginning and the lower hue on the end.
- Saturation: the pixel with the bigger saturation will be at the beginning and the lower saturation at the end.

Detection type: the attribute on which the smart detection’s groups are created.

Lightness Ranges: for smart detection using lightness range as a type. Determine which range need to be sorted (from 0 to 100).

### Algorithms - Slim

Take a pixel color and copy it the one or more pixels in a direction.

Slim direction: the direction on which the pixel color is copied.

Probability's area of effect:

- Global: all color has the same probability to be slimed.
- Per color: each color has a given probability to be slimed.

Probability: the probability in percent for each pixel to be slimed.

Affected colors: the colors that are slimed, when a color is unchecked a pixel with this color is never copied to the next one.

### Algorithms - Wrong filter

Force the png encoder to use a wrong filter

Filter: Png filter used when writing the image

## Sampler

The sampler screen helps you to apply all algorithms with default values to on the specific image.

This is the perfect way to create a sample for one image to see which kind of aesthetic you want to explore.

The default values of each algorithm are the one predefines in the glitcher tab.

### Input

As for the glitcher screen this section is for selecting the image you want to glitch. This time only one PNG image can be selected.

### Output

This section again is very similar than the glitcher screen.

The only difference is for the Schema for output filename, the sample will append to the file name you chose the algorithm and filter or direction used for creating the image. That way you can easily know which output has been generated with which algorithm.

### Algorithms

Select which algorithms to use in this sample, only the checked one will be used.

## Converter

A very minimal JPG to PNG converter

The input and output section is similar to the Glitcher tab, the only difference is the input file format. Here JPG image or folder containing JPEG images should be used.
