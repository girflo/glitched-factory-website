+++
title = "Examples"
weight = 4
[extra]
description = "Images and videos created using the Glitched Factory"
+++

{{ image(image_path="/images/examples/duplicate_avg", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/duplicate_paeth", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/duplicate_up", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/replace_avg", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/replace_none", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/replace_paeth", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/replace_up", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/wrong_filter_avg", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/wrong_filter_paeth", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/wrong_filter_sub", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/wrong_filter_up", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/brush_bottom_to_top", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/brush_right_to_left", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/slim_bottom_to_top", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/sort_bottom_to_top", alt="Example of glitched images from the Glitched Factory") }}
{{ image(image_path="/images/examples/sort_left_to_right", alt="Example of glitched images from the Glitched Factory") }}
