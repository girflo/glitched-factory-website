+++
title = "About"
weight = 1
[extra]
description = "what is this website ?"
+++

Glitched Factory is both a software you can use and an art project of two persons, a video editor and a developer. The core idea is to promote glitch art and to make it accessible to everyone.

## Software

Packed with everything to create glitched images in a couple of seconds, anyone can use it and can master it quickly. The software is free, open source and available for Linux, Windows and Macos. 

It also contains a documentation embeded in the software so you don't need to come back to this website for more information.

We arec looking forward to see how you use the software in your creations.

## Art project

All the images and videos posted on this website, instagram or mastodon are created using the software in some part of the process. We tried to showcase different use of our software but also to experiment using the glitch art aestetic.


## Website

The glitch effect on the homepage headline comes from [this website](https://codepen.io/mattgrosswork/pen/VwprebG).
[PicNic](https://velvetyne.fr/fonts/picnic/) from velvetyne is the font used in the logo and on the Itch.io page.
