+++
title = "Rotating"
date = "2020-06-02"
[extra]
image = "/images/projects/space-guts/rotating/5.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/fDav68viS3k") }}
{{ image(image_path="/images/projects/space-guts/rotating/1", alt="Space guts 1") }}
{{ image(image_path="/images/projects/space-guts/rotating/2", alt="Space guts 2") }}
{{ image(image_path="/images/projects/space-guts/rotating/3", alt="Space guts 3") }}
{{ image(image_path="/images/projects/space-guts/rotating/4", alt="Space guts 4") }}
{{ image(image_path="/images/projects/space-guts/rotating/5", alt="Space guts 5") }}
{{ image(image_path="/images/projects/space-guts/rotating/6", alt="Space guts 6") }}
{{ image(image_path="/images/projects/space-guts/rotating/7", alt="Space guts 7") }}
