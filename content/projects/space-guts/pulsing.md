+++
title = "Pulsing"
date = "2020-06-01"
[extra]
image = "/images/projects/space-guts/pulsing/5.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/vV8pRn3xD2A") }}
{{ image(image_path="/images/projects/space-guts/pulsing/1", alt="Space guts 1") }}
{{ image(image_path="/images/projects/space-guts/pulsing/2", alt="Space guts 2") }}
{{ image(image_path="/images/projects/space-guts/pulsing/3", alt="Space guts 3") }}
{{ image(image_path="/images/projects/space-guts/pulsing/4", alt="Space guts 4") }}
{{ image(image_path="/images/projects/space-guts/pulsing/5", alt="Space guts 5") }}
{{ image(image_path="/images/projects/space-guts/pulsing/6", alt="Space guts 6") }}
{{ image(image_path="/images/projects/space-guts/pulsing/7", alt="Space guts 7") }}
