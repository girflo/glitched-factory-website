+++
title = "Falling"
date = "2020-06-02"
[extra]
image = "/images/projects/space-guts/falling/5.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/yh2ytFYfEQ0") }}
{{ image(image_path="/images/projects/space-guts/falling/1", alt="Space guts 1") }}
{{ image(image_path="/images/projects/space-guts/falling/2", alt="Space guts 2") }}
{{ image(image_path="/images/projects/space-guts/falling/3", alt="Space guts 3") }}
{{ image(image_path="/images/projects/space-guts/falling/4", alt="Space guts 4") }}
{{ image(image_path="/images/projects/space-guts/falling/5", alt="Space guts 5") }}
{{ image(image_path="/images/projects/space-guts/falling/6", alt="Space guts 6") }}
{{ image(image_path="/images/projects/space-guts/falling/7", alt="Space guts 7") }}
