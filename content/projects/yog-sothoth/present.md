+++
title = "Present"
date = "2020-05-26"
[extra]
image = "/images/projects/yog-sothoth/present/2.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/KAkg-9rQGlA") }}
{{ image(image_path="/images/projects/yog-sothoth/present/1", alt="Yog Sothoth 1") }}
{{ image(image_path="/images/projects/yog-sothoth/present/2", alt="Yog Sothoth 2") }}
{{ image(image_path="/images/projects/yog-sothoth/present/3", alt="Yog Sothoth 3") }}
