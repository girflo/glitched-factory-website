+++
title = "Past"
date = "2020-05-25"
[extra]
image = "/images/projects/yog-sothoth/past/2.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/YfAQKaUArfc") }}
{{ image(image_path="/images/projects/yog-sothoth/past/1", alt="Yog Sothoth 1") }}
{{ image(image_path="/images/projects/yog-sothoth/past/2", alt="Yog Sothoth 2") }}
{{ image(image_path="/images/projects/yog-sothoth/past/3", alt="Yog Sothoth 3") }}
