+++
title = "Future"
date = "2020-05-27"
[extra]
image = "/images/projects/yog-sothoth/future/2.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/46H8_jNZTs4") }}
{{ image(image_path="/images/projects/yog-sothoth/future/1", alt="Yog Sothoth 1") }}
{{ image(image_path="/images/projects/yog-sothoth/future/2", alt="Yog Sothoth 2") }}
{{ image(image_path="/images/projects/yog-sothoth/future/3", alt="Yog Sothoth 3") }}
