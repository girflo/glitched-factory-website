+++
title = "Crystal"
date = "2020-10-25"
[extra]
image = "/images/projects/glitchtober-2020/25.CRYSTAL/3.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/PRkIWODgymQ") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/1", alt="Crystal 1") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/2", alt="Crystal 2") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/3", alt="Crystal 3") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/4", alt="Crystal 4") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/5", alt="Crystal 5") }}
{{ image(image_path="/images/projects/glitchtober-2020/25.CRYSTAL/6", alt="Crystal 6") }}
