+++
title = "Mask"
date = "2020-10-14"
[extra]
image = "/images/projects/glitchtober-2020/14.MASK/2.webP"
+++

{{ image(image_path="/images/projects/glitchtober-2020/14.MASK/1", alt="Mask 1") }}
{{ image(image_path="/images/projects/glitchtober-2020/14.MASK/2", alt="Mask 2") }}
