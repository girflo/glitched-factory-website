+++
title = "Dissolve"
date = "2020-10-20"
[extra]
image = "/images/projects/glitchtober-2020/20.DISSOLVE/4.webP"
+++

{{ image(image_path="/images/projects/glitchtober-2020/20.DISSOLVE/1", alt="Dissolve 1") }}
{{ image(image_path="/images/projects/glitchtober-2020/20.DISSOLVE/2", alt="Dissolve 2") }}
{{ image(image_path="/images/projects/glitchtober-2020/20.DISSOLVE/3", alt="Dissolve 3") }}
{{ image(image_path="/images/projects/glitchtober-2020/20.DISSOLVE/4", alt="Dissolve 4") }}
{{ image(image_path="/images/projects/glitchtober-2020/20.DISSOLVE/5", alt="Dissolve 5") }}
