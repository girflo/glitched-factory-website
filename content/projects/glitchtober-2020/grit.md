+++
title = "Grit"
date = "2020-10-02"
[extra]
image = "/images/projects/glitchtober-2020/02.GRIT/GRIT_1.webP"
+++

{{ image(image_path="/images/projects/glitchtober-2020/02.GRIT/GRIT_1", alt="Grit 1") }}
{{ image(image_path="/images/projects/glitchtober-2020/02.GRIT/GRIT_2", alt="Grit 2") }}
