+++
title = "CONCEPTUAL - Red Sun"
date = "2020-06-05"
[extra]
image = "/images/projects/clips/red-sun/5.webP"
+++

{{ youtube_embed(youtube_url="https://www.youtube-nocookie.com/embed/79T8c0vW0ik") }}
{{ image(image_path="/images/projects/clips/red-sun/1", alt="Red sun 1") }}
{{ image(image_path="/images/projects/clips/red-sun/2", alt="Red sun 2") }}
{{ image(image_path="/images/projects/clips/red-sun/3", alt="Red sun 3") }}
{{ image(image_path="/images/projects/clips/red-sun/5", alt="Red sun 5") }}
{{ image(image_path="/images/projects/clips/red-sun/6", alt="Red sun 6") }}
{{ image(image_path="/images/projects/clips/red-sun/7", alt="Red sun 7") }}
{{ image(image_path="/images/projects/clips/red-sun/8", alt="Red sun 8") }}
